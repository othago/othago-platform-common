package io.othago.platform.common.logging

import io.othago.platform.common.integration.logging.MicroutilsKotlinLogger

actual object PlatformLoggerFactory {
    actual fun logger(id: String?): IPlatformLogger = MicroutilsKotlinLogger.retLogger(id!!)
}