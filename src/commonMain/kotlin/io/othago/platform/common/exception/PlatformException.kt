/*
 * Copyright 2022 Othago Software (author: Piotr Migda)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.common.exception

import kotlinx.collections.immutable.toImmutableMap

open class PlatformException : Throwable {

    val code: IPlatformExceptionCode
    private val paramMap = mutableMapOf<String, String>()

    constructor(msg: String = "UNKNOWN_MESSAGE", code: IPlatformExceptionCode = PlatformExceptionCode.PLF_GENERIC, params: Map<String,String> = emptyMap()) : super(msg) {
        this.code = code
        this.paramMap.putAll(params)
    }

    constructor(msg: String, tx: Throwable, code: IPlatformExceptionCode = PlatformExceptionCode.PLF_GENERIC, params: Map<String,String> = emptyMap()) : super(msg, tx) {
        this.code = code
        this.paramMap.putAll(params)
    }

    fun bind(key: String, value: String) = paramMap.put(key,value)
    fun bind(params : Map<String,String>) = paramMap.putAll(params)

    fun getMsg() = message ?: "UNKNOWN_MESSAGE"
    fun retParamMap() = paramMap.toImmutableMap()
    //fun retParamList() = retParamMap().map { ErrorParam(it.key, it.value) }
    //fun retParam(key: String) = retParamMap()[key]

    /*open fun toErrorResult(): ErrorResult = ErrorResult(
        ErrorItem(this.code.toResultObject(), this.getMsg(), this.retParamList())
    )*/
}
