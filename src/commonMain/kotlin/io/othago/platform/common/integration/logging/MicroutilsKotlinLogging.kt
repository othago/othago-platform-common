/*
 * Copyright 2022 Othago Software (author: Piotr Migda)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.common.integration.logging

import io.othago.platform.common.logging.IPlatformLogger
import mu.KLogger
import mu.KotlinLogging

/* adapter: available for JVM / JS / Native[Linux] */
class MicroutilsKotlinLogger private constructor(private val logger: KLogger) : IPlatformLogger {

    companion object {
        fun retLogger(id: String): IPlatformLogger = MicroutilsKotlinLogger(KotlinLogging.logger(id))
    }

    override fun error(msg: String) = logger.error { msg }
    override fun error(msg: String, ex: Throwable) = logger.error(ex) { msg }
    override fun warn(msg: String) = logger.warn { msg }
    override fun warn(msg: String, ex: Throwable) = logger.warn(ex) { msg }
    override fun info(msg: String) = logger.info { msg }
    override fun info(msg: String, ex: Throwable) = logger.info(ex) { msg }
    override fun debug(msg: String) = logger.debug { msg }
    override fun debug(msg: String, ex: Throwable)  = logger.debug(ex) { msg }
    override fun trace(msg: String) = logger.trace { msg }
    override fun trace(msg: String, ex: Throwable) = logger.trace(ex) { msg }

}
