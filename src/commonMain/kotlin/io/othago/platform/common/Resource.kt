/*
 * Copyright 2022 Othago Software (author: Piotr Migda)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.common

import io.othago.platform.common.assert.InfrastructureValidationAssert
import io.othago.platform.common.assert.PlatformCommonAssert
import io.othago.platform.common.logging.PlatformLoggerFactory.logger
import kotlinx.collections.immutable.toImmutableList
import kotlinx.collections.immutable.toImmutableMap

interface IPlatformResource<ID : Comparable<ID>> {
    fun retId(): ID
    fun sig(): String = "${this::class.simpleName}:${retId()}"
}

open class ResourceRegistry<ID : Comparable<ID>, R : IPlatformResource<ID>>(val registryName: String) {

    companion object {
        protected val log = logger(this::class.simpleName!!)
    }

    private val registry = mutableMapOf<ID, R>()

    fun sig() = "${this::class.simpleName}:${registryName}"

    fun registerResource(res: R) {
        PlatformCommonAssert.assertNotContains(registry, res.retId(), "(${sig()})|Resource already registered|[res=${res.sig()}]")
        registry[res.retId()] = res
        log.info("(${sig()})|Resource registered|[res=${res.sig()}]")
    }

    fun getResource(resId: ID): R? = registry[resId]

    fun retResource(resId: ID): R {
        val res = getResource(resId)
        InfrastructureValidationAssert.assertNotNull(res, "(${sig()})|No resource found|[resId=$resId]")
        return res!!
    }

    fun retResourceMap(): Map<ID, R> = registry.toImmutableMap()
    fun retResourceList(): List<R> = registry.values.sortedWith { a, b -> a.retId().compareTo(b.retId()) }.toImmutableList()
}
