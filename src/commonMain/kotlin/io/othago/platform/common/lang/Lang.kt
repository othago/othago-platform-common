/*
 * Copyright 2022 Othago Software (author: Piotr Migda)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.common.lang

import io.othago.platform.common.assert.InfrastructureConfigurationCodeAssert


fun <T : Any?> T?.isNotNull(): Boolean = this != null
fun <T : Any?> T?.isNull(): Boolean = this == null

fun <K : Any, V : Any, M : Map<K, V>> M.ret(key: K): V {
    InfrastructureConfigurationCodeAssert.assertContains(this, key, "Map doesn't contain key : $key")
    return this[key]!!
}

inline fun  <K : Any, reified V : Any, M : Map<K, *>> M.retVal(key: K): V {
    val res = this[key]
    InfrastructureConfigurationCodeAssert.assertNotNull(res, "Map doesn't contain value for key : $key")
    InfrastructureConfigurationCodeAssert.assertTrue(
        res is V,
        "Type of value inserted into Map [${res!!::class.simpleName!!}] is different than expected while performing retrieval [${V::class.simpleName!!}]"
    )
    return res as V
}

inline fun  <reified V : Any, M : Map<*, Any>> M.retVal(): V {
    val res = values.firstOrNull{ it is V }
    InfrastructureConfigurationCodeAssert.assertNotNull(res, "Map doesn't contain value of class : ${V::class.simpleName!!}")
    return res as V
}

inline fun  <K : Any, reified V : Any?, M : Map<K, *>> M.getVal(key: K): V? {
    val res = this[key]
    InfrastructureConfigurationCodeAssert.assertTrue(
        res is V?,
        "Type of value inserted into Map [${res!!::class.simpleName!!}] is different than expected while performing retrieval [${V::class.simpleName!!}]"
    )
    return res as V?
}

inline fun <reified T> LST(vararg elements: T): List<T> = elements.toList()
inline fun <reified K, reified V> MAP(vararg elements: Pair<K,V>): Map<K,V> = elements.toList().toMap()

