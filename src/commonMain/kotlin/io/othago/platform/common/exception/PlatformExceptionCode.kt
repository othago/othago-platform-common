/*
 * Copyright 2022 Othago Software (author: Piotr Migda)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.common.exception

/* Enum doesn't provide inheritance, interface is used to define module / features exception code definition classes
 * that can be populated to PlatformException descendants  */
interface IPlatformExceptionCode {
    val code: String
    val desc: String
}

/* Exception Code
   format is: "(PLF|INF|BUS):[A-Z]{3}-[0-9]{4}"
   where:
     + PLF (Platform), INF (Infrastructure), BUS (Business) -> exception root family
     + [A-Z]{3} -> subfamily code
     + [0-9]{4} -> number, space is 9999 exceptions, 0000 is always reserved for subfamily generic
*/

/* Rule is to:

   - create specific instances / objects of PLF / INF exception family with specific cause msg
     i.e. don't subclass new exception class for each exceptional situation,
     no need to translate each of those with i18n mechanism, mostly for platform backend / service logging
     for each of major exception classes dedicated common assert class is created

   - subclass  / define new PlatformExceptionCodes for business related exceptional situations
     i.e. create dedcated exception code class for each distinct exceptional condition
     i18n translation will be provided for display purposes and exception code is a correlator
     for translation wording matching
     no need to create new PlatformBusiness exceptions subclasses (only pass distinct codes and bind params)

*/
enum class PlatformExceptionCode(override val code: String, override val desc: String) : IPlatformExceptionCode {
    PLF_GENERIC("PLF:GNR-0000", "Platform root for exceptions hierarchy"),
    PLF_COMMON("PLF:CMN-0000", "Exception in platform common / lang util classes"),
    INF_GENERIC("INF:GNR-0000", "Root for technical / infrastructure exceptions subtree"),
    INF_VALID_GENERIC("INF:VLD-0000", "Technical / infrastructure validation exceptions"),
    INF_CORE_GENERIC("INF:COR-0000", "Subtree for all classes of infrastructure core functionality exceptions"),
    INF_CORE_UNSUPPORTED("INF:COR-0001", "Unsupported / unimplemented logic exception"),
    INF_CONF_GENERIC("INF:CFG-0000", "Infrastructure / platform configuration exceptions subtree"),
    INF_CONF_CODE("INF:CFG-0001", "Infrastructure configuration subclass : source code / programming issues"),
    INF_CONF_DI("INF:CFG-0002", "Infrastructure configuration subclass : dependency injection issues"),
    INF_SERVLET_GENERIC("INF:SVT-0000", "Servlet related exceptions family"),
    INF_DATA_GENERIC("INF:DAT-0000", "Data access technical / infrastructure related exceptions"),
    INF_DATA_OPTIMISTIC_LOCK("INF:DAT-0001", "Optimistic locking / infrastructure validation exceptions"),
    INF_DATA_NO_CHANGES("INF:DAT-0002", "No changes in data item detected"),
    INF_DATA_ACCESS("INF:DAT-0003", "Data access exception, infrastructure / technical exceptions"),
    INF_AUTH_GENERIC("INF:ATH-0000", "Authentication / Authorization exceptions class family"),
    INF_AUTH_UNAUTHORIZED("INF:ATH-0001", "Not authenticated"),
    INF_AUTH_FORBIDDEN("INF:ATH-0002", "Not authorized to access resource / execute action"),
    INF_AUTH_JWT_GENERIC("INF:ATH-0100", "JWT Authentication / Authorization exceptions class family"),
    INF_AUTH_JWT_NO_ACCESS_TOKEN("INF:ATH-0101", "No JWT access token"),
    INF_AUTH_JWT_ACCESS_TOKEN_EXPIRED("INF:ATH-0102", "JWT access token expired"),
    INF_AUTH_JWT_REFRESH_TOKEN_EXPIRED("INF:ATH-0103", "JWT refresh token expired"),

    BUS_GENERIC("BUS:GNR-0000", "Root for business / domain validation exceptions subtree"),
    BUS_VALID_GENERIC("BUS:VLD-0000", "Business validation exceptions subtree"),
    BUS_VALID_FORM_FIELD("BUS:VLD-0001", "Business validation exception family of form fields"),
    BUS_DATA_GENERIC("BUS:DAT-0000", "Business data and relationships related exceptions"),
    BUS_DATA_INTEGRITY("BUS:DAT-0001", "Data integrity, constraints violation"),
    BUS_DATA_CONSTRAINT_VIOLATION("BUS:DAT-0002", "Constraints violation"),
    BUS_DATA_RETRIEVAL("BUS:DAT-0003", "Retrieval failure, requested data / item by id not found.") //doesn't apply for empty data set / list which is regular situation

}

