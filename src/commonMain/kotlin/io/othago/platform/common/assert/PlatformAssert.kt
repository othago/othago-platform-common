/*
 * Copyright 2022 Othago Software (author: Piotr Migda)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.common.assert

import io.othago.platform.common.exception.*
import io.othago.platform.common.lang.isNotNull
import io.othago.platform.common.lang.isNull

abstract class PlatformAssert<EX : PlatformException> (
    private val exFactory: (String, PlatformExceptionCode, Map<String,String>) -> EX,
    protected val defaultExceptionCode: PlatformExceptionCode
){

    protected fun check(cond : Boolean, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) {
        if (!cond) {
            throw exFactory.invoke(msg, code, params)
        }
    }

    fun assertNull(obj: Any?, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(obj.isNull(),msg, code, params)
    fun assertNotNull(obj: Any?, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(obj.isNotNull(),msg, code, params)
    fun assertTrue(condition: Boolean, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(condition,msg, code, params)
    fun assertFalse(condition: Boolean, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(!condition,msg, code, params)
    fun assertEquals(objA: Any?, objB: Any?, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(objA == objB, msg, code, params)
    fun <E : Any?> assertContains(list: List<E>, element: E, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(list.contains(element), msg, code, params)
    fun <E : Any?> assertNotContains(list: List<E>, element: E, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(!list.contains(element), msg, code, params)
    fun <K : Any?> assertContains(map: Map<K, *>, key: K, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(map.contains(key), msg, code, params)
    fun <K : Any?> assertNotContains(map: Map<K, *>, key: K, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(!map.contains(key), msg, code, params)
    fun assertEmpty(collection: Collection<*>, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(collection.isEmpty(), msg, code, params)
    fun assertNotEmpty(collection: Collection<*>, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(collection.isNotEmpty(), msg, code, params)
    fun assertBlank(obj: String?, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(obj.isNullOrBlank(), msg, code, params)
    fun assertNotBlank(obj: String?, msg: String, code: PlatformExceptionCode = defaultExceptionCode, params: Map<String,String> = emptyMap()) = check(!obj.isNullOrBlank(), msg, code, params)

}

object PlatformCommonAssert : PlatformAssert<PlatformCommonException>(
    { m, c, p -> PlatformCommonException(m, c, p) },
    PlatformExceptionCode.PLF_COMMON
)

object InfrastructureValidationAssert : PlatformAssert<InfrastructureValidationException>(
    { m, c, p -> InfrastructureValidationException(m, c, p) },
    PlatformExceptionCode.INF_VALID_GENERIC
)

object InfrastructureConfigurationAssert : PlatformAssert<InfrastructureConfigurationException>(
    { m, c, p -> InfrastructureConfigurationException(m, c, p) },
    PlatformExceptionCode.INF_CONF_GENERIC
)

object InfrastructureConfigurationCodeAssert : PlatformAssert<InfrastructureConfigurationException>(
    { m, c, p -> InfrastructureConfigurationException(m, c, p) },
    PlatformExceptionCode.INF_CONF_CODE
)

object InfrastructureConfigurationDIAssert : PlatformAssert<InfrastructureConfigurationException>(
    { m, c, p -> InfrastructureConfigurationException(m, c, p) },
    PlatformExceptionCode.INF_CONF_DI
)

object BusinessDataIntegrityAssert : PlatformAssert<BusinessDataIntegrityException>(
    { m, c, p -> BusinessDataIntegrityException(m, c, p) },
    PlatformExceptionCode.BUS_DATA_INTEGRITY
)

object BusinessDataRetrievalAssert : PlatformAssert<BusinessDataRetrievalException>(
    { m, c, p -> BusinessDataRetrievalException(m, c, p) },
    PlatformExceptionCode.BUS_DATA_RETRIEVAL
)

object BusinessValidationAssert : PlatformAssert<BusinessValidationException>(
    { m, c, p -> BusinessValidationException(m, c, p) },
    PlatformExceptionCode.BUS_VALID_GENERIC
)

/* additional param required, dedicated assert to enforce compiler to guard api / function signatures*/
object BusinessFieldAssert{
    private val defaultExceptionCode = PlatformExceptionCode.BUS_VALID_FORM_FIELD

    private fun check(cond : Boolean, fieldId: String, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (!cond) {
            throw BusinessValidationFieldException(fieldId, msg, code)
        }
    }

    fun assertNull(obj: Any?, fieldId: String, msg: String, code: PlatformExceptionCode = defaultExceptionCode) = check(obj.isNull(),fieldId,msg,code)
    fun assertNotNull(obj: Any?, fieldId: String, msg: String, code: PlatformExceptionCode = defaultExceptionCode) = check(obj.isNotNull(),fieldId,msg,code)
    fun assertTrue(condition: Boolean, fieldId: String, msg: String, code: PlatformExceptionCode = defaultExceptionCode) = check(condition,fieldId,msg,code)
    fun assertFalse(condition: Boolean, fieldId: String, msg: String, code: PlatformExceptionCode = defaultExceptionCode) = check(!condition,fieldId,msg,code)
}
